var express = require('express');
var router = express.Router();
const User = require('../models/user');
const Post = require('../models/post');
/* GET all users list. */
router.get('/', function(req, res, next) {
  User.find({}).then(doc => {
    res.send({
      data: doc,
      status: 'Ok'
    });
  }).catch(err => {
    console.error(err);
  });
});

// GET all User Post
router.get('/allPost/:username',function (req, res) {
  userName = req.params.username;
  User.findOne({username: userName})
  .populate('posts').exec((err, posts) => {
    console.log('Populated User ' + posts);
    res.send(posts);
  });

});

// POST user Post Content
router.post('/create/blogpost', function (req, res, next) {
  let userName = req.body.username;
  let post = req.body.content;
  User.findOne({username: userName}, function (err, user) {
    console.log(user);
    if (!err){
      post = new Post({
        'content': post,
        'author': user._id
      });
      post.save(function(err) {
        if (err) {
          throw err;
          }
        console.log('Post created');
      });
      user.posts.push(post._id);
      user.save(function(err) {
        if (err)  {
          throw err;
        }
        console.log('User successfully updated!');
        res.send();
      });
    } else {
      console.log(err);
    }
  });

});
// POST create User
router.post('/create/user', function(req,res,next){
  let userName = req.body.username;
  User.create(req.body).then(task => {
      res.send(200, task);
      next();
  })
  .catch(err => {
      res.send(500, err);
  });
});

module.exports = router;

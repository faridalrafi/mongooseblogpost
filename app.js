var express = require('express');
// var path = require('path');

const mongoose = require('mongoose');
const createError = require('http-errors');
const logger = require('morgan');
var users = require('./routes/users');
var app = express();
const devDbUrl = 'mongodb://someuser:abcd1234@ds053194.mlab.com:53194/productstutorial';
// const uri = "mongodb+srv://faridrafi:1234@cluster0-nr3ar.mongodb.net/blog?retryWrites=true";
const mongoDB = process.env.MONGODB_URI || devDbUrl;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(logger('dev'));


app.use('/api/v1/users', users);

module.exports = app;
